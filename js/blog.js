const navButton = document.querySelector('button#toggle');
const navbar = document.querySelector('.navbar');
const navbarTrans = document.querySelector('.navbar.transparent');
const sidebar = document.querySelector('.sidebar');
const navbarProfile = document.querySelector('.profile .button');
const navbarProfileContent = document.querySelector('.profile .profile-content');
const newsthumbnail = document.querySelector('.news-thumbnail');
const body = document.querySelector('body');

const modal = document.querySelector('.modal');

let navButtonStat = false;

navButton.addEventListener('click', ()=>{
    if(!navButtonStat){
        navButton.classList.add('open');
        navbar.classList.add('open');
        sidebar.classList.add('open');
        body.classList.add('overflow');
        navButtonStat = true;
    } else {
        navButton.classList.remove('open');
        navbar.classList.remove('open');
        sidebar.classList.remove('open');
        body.classList.remove('overflow');
        navButtonStat = false;
    }
});

let navbarProfileStat = false;

// navbarProfile.addEventListener('click', ()=>{
//     if(!navbarProfileStat){
//         navbarProfileContent.classList.add('open');
//         navbarProfileStat = true;
//     } else {
//         navbarProfileContent.classList.remove('open');
//         navbarProfileStat = false;
//     }
// });

// function modal_id(x){
//     var y = document.getElementById(x);
//     y.classList.add("open");
// }

window.onclick = function(event){
    if(event.target == modal){
        modal.classList.remove("open");
    }
}

// window.onscroll = function() {navbarScroll()};

// function navbarScroll(){
//     if(document.body.scrollTop > 75 || document.documentElement.scrollTop > 75){
//         navbarTrans.classList.remove("transparent");
//     }else{
//         navbarTrans.classList.add("transparent");
//     }
// }